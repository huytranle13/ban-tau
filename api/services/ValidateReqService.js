module.exports = {
    validateInputLogin: function(req) {
      // here you call your models, add object security validation, etc...
        return req.validate([   // 'numeric || string' or 'numeric|| string' are OK. Space will be ignored
            {'user_name': 'string'},
            {'password':'string'}
        ]);
    },
    validateInputSignup: function(req) {
        // here you call your models, add object security validation, etc...
          return req.validate([   // 'numeric || string' or 'numeric|| string' are OK. Space will be ignored
              {'user_name': 'string'},
              {'password':'string'},
              {"Name":"string"},
              {"Email":"email || string"},
              {"Phone":"numeric"}
          ]);
    },
    validateInputOtp:function(req){
        return req.validate([   // 'numeric || string' or 'numeric|| string' are OK. Space will be ignored
              {'token': 'string'},
          ]);
    }
};