/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    login:async function(req,res) {

        //validate input
        var params = ValidateReqService.validateInputLogin(req);        
        if(!params)
            return ;
        
        //check Username hoặc email có trong DB
        var oResult = await Users.getUserLoginInfo(params);

        if(oResult == false) {
            return res.status(401).json({
                'status':"error",
                'message':"User không tồn tại"
            });
        }else if(typeof oResult == "string"){
            return res.status(401).json({
                'status':"error",
                'message':oResult
            });
        }

        //check Password
        await sails.helpers.passwords.checkPassword(params.password, oResult.password)
                    .intercept("incorrect",function(){
                        return res.status(401).json({
                            status:'error',
                            message:'Sai mật khẩu'
                        });
                    });

        // thay doi Login Status
        await Users.updateUser(oResult.user_id,{
            isLogin:1
        });

        // tao jwt
        var jwt = JwtService.jwtSign({
            'userid':oResult.user_id,
            'username':oResult.user_name,
            'name':oResult.Name
        });
        
        return res.status(200).json({
            'status':"success",
            'data':{
                'jwt_string':jwt
            }
        });
    },
    logout:async function(req,res){
        var loginstatus = await Users.checkIfLogin(req.users.userid);
        if(loginstatus==false){
            return res.status(401).json({
                status:"error",
                message:"User vẫn chưa đăng nhập"
            });
        }

        try{
            await Users.updateUserLogin(req.users.userid,0);
            return res.status(200).json({
                status:"success"
            });
        }catch(err){
            console.log(err);
            return res.status(500).json({
                status:"error",
                message:err
            });
        }
    }

};

