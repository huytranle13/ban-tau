module.exports = {
    tableName: 'users',
    primaryKey: 'user_id',
    attributes:{
        user_id:{ type:'number',unique:true,autoIncrement:true},
        user_name:{ type:"string",unique:true, required: true,maxLength:256 },
        password: { type: 'string',required: true,protect: true,maxLength:256 },
        Name:{ type:"string",maxLength:256 },
        // Email:{ type:"string",unique:true,isEmail: true,maxLength:256 },
        // Phone:{ type:"string" },
        // shop_token:{ type:'string',maxLength:100 },
        // isShopping:{ type:"number" },
        isLogin:{ type:"number" },
        active:{ type:"number" },
    },
    getUserLoginInfo:async function(params) {
        var oUser = await Users.findOne({
            "select": ['user_name',"Name","password","user_id","active"],
          }).where({ "user_name": params.user_name});

          if(oUser){
            if(Object.getOwnPropertyNames(oUser).length !==0 && oUser.active ==1){
                return oUser;
            }else if(Object.getOwnPropertyNames(oUser).length !==0 && oUser.active ==0){
                return "User chưa được active";
            }
          }else
            return false;
    },

    checkIfLogin:async function(userid){
        var oUser = await Users.findOne({
            select: ['isLogin','active'],
          }).where({'user_id': userid});
        
        if(Object.getOwnPropertyNames(oUser).length !=0 && oUser.active ==1 && oUser.isLogin==1){
            return true;
        }
        else if(Object.getOwnPropertyNames(oUser).length !=0 && oUser.active ==1 && oUser.isLogin==0)
            return false;
        else if(Object.getOwnPropertyNames(oUser).length !=0 && oUser.active ==0)
            return "User chưa được active";
        else
            return false;
    },

    updateUser:async function(userid,user){
        await Users.update({user_id:userid})
                    .set(user);
    },

    insertUser : async function(user,db){
        var user = await Users.create(user).usingConnection(db).fetch();
        return user;
    },

    // getShoppingToken:async function(userid){
    //     var user = await Users.find({
    //         where: { user_id: userid },
    //         select: ['shop_token']
    //       }).limit(1);
    //     if(user[0])
    //         return user[0].shop_token;
    //     else
    //         return false;
        
    // },

    // checkShoppingToken:async function(shoptoken){
    //     var user = await Users.find({
    //         where: { shop_token: shoptoken },
    //         select: ['user_name',"Name","user_id"]
    //       }).limit(1);
    //     if(user[0])
    //         return user[0];
    //     else
    //         return false;
    // },

    // changeShoppingStatus:async function(userid,status){
    //     try{
    //         await Users.update({user_id:userid})
    //                     .set({isShopping:status});
    //         return true;
    //     }catch(err){
    //         console.log(err);
    //         return false;
    //     }
    // }
}